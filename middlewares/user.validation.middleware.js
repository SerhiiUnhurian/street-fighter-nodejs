const Joi = require("joi");

const baseSchema = {
  firstName: Joi.string().min(2).max(20),
  lastName: Joi.string().min(2).max(20),
  email: Joi.string().pattern(/^[-.\w]+@gmail.com$/),
  phoneNumber: Joi.string().pattern(/^\+380[0-9]{9}$/),
  password: Joi.string().min(5).max(30),
};

const createUserValid = (req, res, next) => {
  const schema = Joi.object().keys(baseSchema);

  const { error } = schema.validate(req.body, {
    presence: "required",
    abortEarly: false,
  });

  if (error)
    return res.status(400).send({
      error: true,
      message: error.message,
    });

  next();
};

const updateUserValid = (req, res, next) => {
  const schema = Joi.object()
    .keys(baseSchema)
    .or("firstName", "lastName", "email", "phoneNumber", "password");

  const { error } = schema.validate(req.body, { abortEarly: false });
  if (error)
    return res.status(400).send({
      error: true,
      message: error.message,
    });

  next();
};

const loginUserValid = (req, res, next) => {
  const schema = Joi.object({
    email: baseSchema.email,
    password: baseSchema.password,
  });

  const { error } = schema.validate(req.body, {
    presence: "required",
    abortEarly: false,
  });

  if (error)
    return res.status(400).send({
      error: true,
      message: error.message,
    });

  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.loginUserValid = loginUserValid;