const Joi = require("joi");

const schema = Joi.object({
  fighter1: Joi.string().guid({ version: "uuidv4" }),
  fighter2: Joi.string().guid({ version: "uuidv4" }),
  log: Joi.array().items(
    Joi.object({
      fighter1Shot: Joi.number(),
      fighter2Shot: Joi.number(),
      fighter1Health: Joi.number(),
      fighter2Health: Joi.number(),
    })
  ),
});

const createFightValid = (req, res, next) => {
  const { error } = schema.validate(req.body, {
    presence: "required",
    abortEarly: false,
  });

  if (error) {
    return res.status(400).send({
      error: true,
      message: error.message,
    });
  }

  next();
};

exports.createFightValid = createFightValid;
