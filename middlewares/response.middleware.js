const responseMiddleware = (req, res, next) => {
  const error = res.err;
  if (error) {
    return res.status(404).send({
      error: true,
      message: error.message,
    });
  }

  res.send(res.data);
};

exports.responseMiddleware = responseMiddleware;
