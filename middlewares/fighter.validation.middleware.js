const Joi = require("joi");

const baseSchema = {
  name: Joi.string().min(2).max(20),
  power: Joi.number().integer().min(1).max(100),
  defense: Joi.number().integer().min(1).max(10),
};

const createFighterValid = (req, res, next) => {
  const schema = Joi.object().keys(baseSchema);
  const { error } = schema.validate(req.body, {
    presence: "required",
    abortEarly: false,
  });

  if (error)
    return res.status(400).send({
      error: true,
      message: error.message,
    });

  next();
};

const updateFighterValid = (req, res, next) => {
  const schema = Joi.object()
    .keys(baseSchema)
    .or("name", "health", "power", "defense");

  const { error } = schema.validate(req.body, { abortEarly: false });
  if (error)
    return res.status(400).send({
      error: true,
      message: error.message,
    });

  next();
};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
