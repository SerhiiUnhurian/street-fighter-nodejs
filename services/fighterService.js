const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  search(search) {
    const item = FighterRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(fighter) {
    fighter.health = 100;
    return FighterRepository.create(fighter);
  }

  update(id, dataToUpdate) {
    return FighterRepository.update(id, dataToUpdate);
  }

  delete(id) {
    return FighterRepository.delete(id);
  }

  listFighters() {
    return FighterRepository.getAll();
  }
}

module.exports = new FighterService();
