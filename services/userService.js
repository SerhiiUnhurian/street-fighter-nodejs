const { UserRepository } = require("../repositories/userRepository");

class UserService {
  search(search) {
    const item = UserRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(user) {
    return UserRepository.create(user);
  }

  update(id, dataToUpdate) {
    return UserRepository.update(id, dataToUpdate);
  }

  delete(id) {
    return UserRepository.delete(id);
  }

  listUsers() {
    return UserRepository.getAll();
  }
}

module.exports = new UserService();
