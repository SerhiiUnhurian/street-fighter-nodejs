const UserService = require("./userService");

class AuthService {
  login(userData) {
    const user = UserService.search((user) => {
      return (
        user.email === userData.email && user.password === userData.password
      );
    });
    return user;
  }
}

module.exports = new AuthService();
