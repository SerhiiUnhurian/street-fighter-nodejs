const { FightRepository } = require("../repositories/fightRepository");

class FightService {
  search(search) {
    const item = FightRepository.getOne(search);
    if (!item) {
      return null;
    }
    return item;
  }

  create(fight) {
    return FightRepository.create(fight);
  }

  delete(id) {
    return FightRepository.delete(id);
  }

  listFights() {
    return FightRepository.getAll();
  }
}

module.exports = new FightService();
