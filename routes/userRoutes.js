const { Router } = require("express");
const UserService = require("../services/userService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createUserValid,
  updateUserValid,
} = require("../middlewares/user.validation.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    res.data = UserService.listUsers();
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;
    const user = UserService.search((user) => user.id === id);
    if (!user) {
      res.err = new Error("User not found");
    } else {
      res.data = user;
    }

    next();
  },
  responseMiddleware
);

router.post(
  "/",
  createUserValid,
  (req, res, next) => {
    const { email } = req.body;
    const user = UserService.search((user) => user.email === email);
    if (user) {
      res.err = new Error("User with such email address already exists");
    } else {
      res.data = UserService.create(req.body);
    }

    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  (req, res, next) => {
    const { id } = req.params;
    const user = UserService.search((user) => user.id === id);
    if (!user) {
      res.err = new Error("User not found");
    } else {
      res.data = UserService.update(id, req.body);
    }

    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;
    const user = UserService.search((user) => user.id === id);
    if (!user) {
      res.err = new Error("User not found");
    } else {
      res.data = UserService.delete(id);
    }

    next();
  },
  responseMiddleware
);

module.exports = router;
