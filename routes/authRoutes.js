const { Router } = require("express");
const AuthService = require("../services/authService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const { loginUserValid } = require("../middlewares/user.validation.middleware");
const router = Router();

router.post(
  "/login",
  loginUserValid,
  (req, res, next) => {
    const user = AuthService.login(req.body);
    if (!user) {
      res.err = new Error("User not found");
    } else {
      res.data = user;
    }

    next();
  },
  responseMiddleware
);

module.exports = router;
