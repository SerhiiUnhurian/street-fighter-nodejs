const FightService = require("../services/fightService");
const { Router } = require("express");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFightValid,
} = require("../middlewares/fight.validation.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    res.data = FightService.listFights();
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;
    const fight = FightService.search((fight) => fight.id === id);
    console.log(fight);
    if (!fight) {
      res.err = new Error("Fight not found");
    } else {
      res.data = fight;
    }

    next();
  },
  responseMiddleware
);

router.post(
  "/",
  createFightValid,
  (req, res, next) => {
    res.data = FightService.create(req.body);
    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;
    const fight = FightService.search((fight) => fight.id === id);
    if (!fight) {
      res.err = new Error("Fight not found");
    } else {
      res.data = FightService.delete(id);
    }

    next();
  },
  responseMiddleware
);

module.exports = router;
