const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
} = require("../middlewares/fighter.validation.middleware");

const router = Router();

router.get(
  "/",
  (req, res, next) => {
    res.data = FighterService.listFighters();
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;
    const fighter = FighterService.search((fighter) => fighter.id === id);
    if (!fighter) {
      res.err = new Error("Fighter not found");
    } else {
      res.data = fighter;
    }

    next();
  },
  responseMiddleware
);

router.post(
  "/",
  createFighterValid,
  (req, res, next) => {
    res.data = FighterService.create(req.body);
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  (req, res, next) => {
    const { id } = req.params;
    const fighter = FighterService.search((fighter) => fighter.id === id);
    if (!fighter) {
      res.err = new Error("Fighter not found");
    } else {
      res.data = FighterService.update(id, req.body);
    }

    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  (req, res, next) => {
    const { id } = req.params;
    const fighter = FighterService.search((fighter) => fighter.id === id);
    if (!fighter) {
      res.err = new Error("Fighter not found");
    } else {
      res.data = FighterService.delete(id);
    }

    next();
  },
  responseMiddleware
);

module.exports = router;
